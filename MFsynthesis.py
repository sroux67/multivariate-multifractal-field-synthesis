#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Script for synthesis of scale invariant monofractal and multifractal
stochastic process in multiple dimensions
Allow the synthesis of multivariate fields with given dependeances.
Contents :

============   Scalar fields

 + Multivariate 1D or 2D scalar processes :

    - synthmrw : synthesis of
        Multi-variate Fractional Brownian Motion (0 < Hurst < 1, lambda=0)
      and
        Multifractal Random Walk (lambda>0) in 1D or 2D
        (circular matrix method used)

    - synthMultiFractalField : synthesis of
        Multi-variate  regularized and stationarized Fractional Brownian Motion
      and
        Multifractal Random Walk in 1D or2D.
        The process are stationary but behave as a Fractional Brownian Motion
        or a Multifractal Random Walk in the inertial range.

    - synthMFOU : synthesis of

    - synthOSGRF : synthesis of
        isotropic (0 < Hurst < 1) and anisotropic 2D Fractional Brownian Motion
      (Operator Scaling Gaussian Random Field from Clausel,Vedel --> ref and
       Operator Scaling Gaussian Random Noise)

 ###
 S.G.  Roux  : May 2021
 Ecole Normale Superieure de Lyon
 Laboratoire de Physique
 CNRS UMR 5672
 ENS de Lyon
 46, allee d'Italie
 69007 Lyon
 stephane.roux@ens-lyon.fr
 ###

"""

# library import
import time
import warnings
import scipy
import numpy as np
import scipy.spatial as scspatial
import scipy.signal as scs

import matplotlib.pyplot as plt
# import MFanalysis_Nov2023 as MFa

from functools import partial
from concurrent.futures import ThreadPoolExecutor


from scipy.fft import fftn as fft
from scipy.fft import ifftn as ifft


# %%
def check_corr_params(Nvar, rho):
    """
    check consistency of  covariance matrix

    Parameters
    ----------
    Nvar : int
        Number of variable..
    rho : array of float
        covariance matrix.

    Returns
    -------
    upper_matrix : array of float
        Upper-triangular Cholesky factor of `rho`.
    test : boolean
        True if rho is a valid covariance matrix.

    ##
    S.G.  Roux, ENS Lyon, May 2021

    """
    test = True
    rho = np.atleast_1d(rho)
    if rho.ndim != 2 and rho.shape[0] != Nvar:
        test = False
        print('rho must be a matrix of size len(H)xlen(H).\n')
    if rho.ndim == 1 and rho.shape[0] > 1:
        test = False
        print('rho must be a scalar as len(H)=1.\n')

    if Nvar > 1:
        # check if rho is symmetic
        all_zeros = np.all(np.triu(rho)-np.tril(rho).transpose() == 0)
        if not all_zeros:
            test = False
            print('rho must be a symmetric matrix.\n')
        # rho must be one on the diagonal
        all_1 = np.all(np.diag(rho) == 1)
        if not all_1:
            test = False
            print('rho must have value 1 on the diagonal.\n')
        # rho must be < 1
        all_inf1 = np.all(np.triu(np.abs(rho))-np.ones((Nvar, Nvar)) <= 0)
        if not all_inf1:
            test = False
            print('all elements of rho must be below lesser or equal to 1.\n')
        upper_matrix = scipy.linalg.cholesky(rho, lower=False)
    else:
        upper_matrix = 0

    return upper_matrix, test


# %%
def checkparams(dim, N, H, lambda2, rho=None, rhomf=None, Ani=[1, 0]):
    '''
    Check input parameter for syntjhesis function


    params= checkparams(dim, N, H, lambda2, rho, rhomf, Ani):

    Parameters
    ----------
    dim : int
        dimension of the output (1 or 2).
    N : int
        number of samples.
    H : float or 1D array of float (of size Nvar)
        Hurst parameter (0 < H < 1).
    lambda2 : float or 1D array of float
        intermittency parameter (positive or null).
        Must have the same size as H.
    rho : array of float, optional
        covariance matrix for the fgn.
        Must be of size (Nvar, Nvar). The default is None.
    rhomf : array of float, optional
        covariance matrix for the fgn multiplicative chaos
        Must be of size (Nvar, Nvar). The default is None.
    Ani : list of size 2 por 4, optional
        Anisotropic parameters  used if dim =2Optional
        Ani=[alphaSS, thetaSS] where
            0 < alphaSS < 2. For alphaSS != 1,
                the space is dilated differently for x and y direction)
            0 < thetaSS < 2*pi. Angle of the generating base.
        The deformation of the space are the same for the two noises.
        if Ani list of size 4 the anisotropic parameters for the two noises.
        Ani=[alphaSS, thetaSS, alphaMF, thetaMF]
        The default is Ani =[1, 0].

    Returns
    -------
    params : TYPE
        DESCRIPTION.

    ##
    S.G.  Roux, ENS Lyon, May 2021

    '''
    test = True
    # check dimension
    if not isinstance(dim, int) or (dim > 2):
        test = False
        print('dim must be 1 or 2.\n')
    # check size
    if not isinstance(N, int) or (N < 1):
        test = False
        print('N must be a positive integer.\n')

    # check consistency: Hurst
    H = np.atleast_1d(H)
    # must be a vector
    if H.ndim > 1:
        test = False
        print('H must be one dimensional.\n')
    # must be > 0
    all_pos = np.all(H > 0)
    if not all_pos:
        test = False
        print('H must be positive.\n')
    # must be < 1
    all_pos = np.all(H < 1)
    if not all_pos:
        test = False
        print('H must be smaller than 1.\n')
    all_right = np.all(H > 0) and np.all(H < 1)
    if not all_right:
        test = False
        print('H must be between O and 1.\n')
    Nvar = H.shape[0]

    # check consistency: lambda2
    lambda2 = np.atleast_1d(lambda2)
    # must be a vector
    if lambda2.ndim > 1:
        test = False
        print('lambda2 must be one dimensional.\n')
    # must be > 0
    all_pos = np.all(lambda2 >= 0)
    if not all_pos:
        test = False
        print('lambda2 must be positive or nul.\n')
    # must have same dimension as H
    if Nvar != lambda2.shape[0]:
        test = False
        print('H and lambda2 must have same size.\n')

    # check correlation matrix consistency
    if rho is not None:
        Upp, test = check_corr_params(Nvar, rho)
    else:  # no correlation
        rho = np.array([[1, 0], [0, 1]])
        Upp = rho

    # check MF correlation matrix consistency
    if rhomf is not None:
        Uppmf, test = check_corr_params(Nvar, rhomf)
    else:
        rhomf = np.array([[1, 0], [0, 1]])
        Uppmf = rhomf

    # check anisotropic parameters
    if Ani[0] <= 0 or Ani[0] >= 2:
        test = False
        print('Anisotropic parameter alpha must be berween 0 and 2.\n')
    alphaSS = Ani[0]
    thetaSS = Ani[1]
    if len(Ani) == 4:
        if Ani[3] < 0 or Ani[3] > 2:
            test = False
            print('Anisotropic parameter alpha must be berween 0 and 2.\n')
        alphaMF = Ani[2]
        thetaMF = Ani[3]
    else:
        alphaMF = alphaSS
        thetaMF = thetaSS

    if test:
        params = {
            "dim": dim,
            "N": N,
            "H": H,
            "Nvar": Nvar,
            "lambda2": lambda2,
            "rho": rho,
            "rhomf": rhomf,
            "Upp": Upp,
            "Uppmf": Uppmf,
            "alphaSS": alphaSS,
            "thetaSS": thetaSS,
            "alphaMF": alphaMF,
            "thetaMF": thetaMF,
        }

    else:
        params = {
            "dim":  0
        }

    return params


# %%
def mom24(scale, signal, axis=0):
    """
    compute moment of order 2 abd 4
    of the increment of size scale on a 1d signal

    Mom2, Mom4 = mom24(scale, signal, axis=0)

    Parameters
    ----------
    scale : int
        scale of the increment.
    signal : array of float of size(N,)
        signal under analysis.
    axis : axes, optional
        Axes of the computation. The default is 0.

    Returns
    -------
    Mom2 : float
        Moment of order 2.
    Mom4 : float
        DESCRIPTION.
        Moment of order 4.

    ##
    S.G.  Roux, ENS Lyon, May 2021
    """
    sigtmp = signal[scale:]-signal[:-scale]

    Mom2 = np.mean(sigtmp**2, axis=axis)
    Mom4 = np.mean(sigtmp**4, axis=axis)

    return Mom2, Mom4


def mom24_2d(scale, signal):
    """
    compute moment of order 2 abd 4
    of the increment of size scale
    in the x, y and diagonal direction of  a 2D signal

    Mom2, Mom4 = mom24_2d(scale, signal)

    Parameters
    ----------
    scale : int
        scale of the increment.
    signal : array of float of size(N,)
        signal under analysis.

    Returns
    -------
    Mom2 : float
        Moment of order 2.
    Mom4 : float
        DESCRIPTION.
        Moment of order 4.

    ##
    S.G.  Roux, ENS Lyon, May 2021

    """
    if signal.ndim == 2:
        Mom2 = np.zeros((3, 1))
        Mom4 = np.zeros((3, 1))
    else:
        Mom2 = np.zeros((3, signal.shape[2]))
        Mom4 = np.zeros((3, signal.shape[2]))

    imageInt = np.roll(signal, scale, axis=0)
    image2 = np.roll(imageInt, scale, axis=1)
    # diagonal
    incr = (image2[scale:-scale, scale:-scale]
            - signal[scale:-scale, scale:-scale])

    Mom2[0, :] = np.mean(np.abs(incr)**2)
    Mom4[0, :] = np.mean(incr**4)
    # x dir
    incr = (imageInt[scale:-scale, scale:-scale]
            - signal[scale:-scale, scale:-scale])
    Mom2[1, :] = np.mean(np.abs(incr)**2)
    Mom4[1, :] = np.mean(incr**4)
    # y dir
    imageInt = np.roll(imageInt, scale, axis=1)
    incr = (imageInt[scale:-scale, scale:-scale]
            - signal[scale:-scale, scale:-scale])
    Mom2[2, :] = np.mean(np.abs(incr)**2)
    Mom4[2, :] = np.mean(incr**4)

    return np.squeeze(Mom2), np.squeeze(Mom4)


# %% Increment analysisis / moment of order 2 and 4
def analyseIncrSimple(signal, nv=2, axis=0, dim=1, workers=1):
    """
    Increment Analysis : compute moment of order 2 and flatness
    of the increment of the signal at different scales.

    Mom2, Mom4, scales = analyseIncrSimple(signal, nv=2, axis=0,
                                           dim=1, workers=1)

    Parameters
    ----------
    signal : array of float of size(N, )
        signal under anlysis.
    nv : int, optional
        number of octave for the scales definition. The default is 2.
    axis : axes, optional
        Axis along the direction the analysis is performed. The default is 0.
    dim : int, optional
        dimension of the signal.  signal can be a collection of 1d signals.
        The default is 1.
    workers : int, optional
        Nuber or threads used in the computation.. The default is 1.

    Raises
    ------
    TypeError
        DESCRIPTION.

    Returns
    -------
    Mom2 : array of float of size (Nscales,)
        Moment of order 2 of the increments.
    Mom4 : array of float of size (Nscales,)
        Moment of order 4 of the increments.
    scales : array of float of size (Nscales,)
        The scales compured.

    ##
    S.G.  Roux, ENS Lyon, December 2020

    """

    # signal = np.squeeze(signal)
    # check parameters
    if signal.ndim > 3:
        raise TypeError('Signal has too much dimension.\n')

    uu = 2 ** np.arange(0, np.log2(len(signal))-3, 1/nv)
    scales = np.unique(uu.astype(int))

    if dim == 1:
        if signal.ndim == 1:
            partialCompute = partial(mom24, signal=signal)
            with ThreadPoolExecutor(max_workers=workers) as executor:
                result_list = executor.map(partialCompute, list(scales))
            vv = np.array(list(result_list))
            Mom2 = vv[:, 0].T
            Mom4 = vv[:, 1].T

        elif signal.ndim == 2:
            # t=time.time()
            partialCompute = partial(mom24, signal=signal, axis=axis)
            with ThreadPoolExecutor(max_workers=workers) as executor:
                result_list = executor.map(partialCompute, list(scales))
            vv = np.array(list(result_list))
            Mom2 = vv[:, 0]
            Mom4 = vv[:, 1]

        else:
            raise TypeError('Not implemented\n')

    # une seule image 2D --> compute moments of diagonal increments
    elif len(signal.shape) == 2:
        partialCompute = partial(mom24_2d, signal=signal)
        with ThreadPoolExecutor(max_workers=workers) as executor:
            result_list = executor.map(partialCompute, list(scales))
        vv = np.array(list(result_list))
        Mom2 = vv[:, 0, :]
        Mom4 = vv[:, 1, :]

    # multi component images
    else:  # multi component images

        partialCompute = partial(mom24_2d, signal=signal)
        # t = time.time()
        with ThreadPoolExecutor(max_workers=workers) as executor:
            result_list = executor.map(partialCompute, list(scales))
        vv = np.array(list(result_list))
        Mom2 = vv[:, 0, :]
        Mom4 = vv[:, 1, :]

        if Mom2.ndim == 2:
            Mom2 = Mom2[:, :, np.newaxis]
            Mom4 = Mom4[:, :, np.newaxis]

        Mom2 = np.transpose(Mom2[np.newaxis, :, :, :], (0, 1, 3, 2))
        Mom4 = np.transpose(Mom4[np.newaxis, :, :, :], (0, 1, 3, 2))

    return Mom2, Mom4, scales


# %%
def analyse1dCorrSpectrum(x, nps=4096):
    """
    Compute autocorrelation function and power spectrum of a 1d signal.

    Parameters
    ----------
    x : array of float
        signal under analysis.
    nps : int, optional
        size of the window for the power welch power spectrum estimation.
        The default is 4096.

    Returns
    -------
    c : array of float
        The auto correlation function.
    cx : array of float
        The lags of the auto correlation function.
    Pxx : array of float
        The power spectrum.
    f : array of float
        The frequencies of the power spectrum.

    ##
    S.G.  Roux, ENS Lyon, December 2020

    """

    if x.ndim == 2:
        N, Nvar = x.shape
    else:
        N = x.shape[0]
        Nvar = 1

    cx = np.arange(-N+1, N)

    # Initialization of Matrix
    c = np.zeros((Nvar, len(cx)))
    Pxx = np.zeros((Nvar, int(nps/2+1)))

    for ivar in range(Nvar):
        # estimation using correlation of noise
        xtemp = (x[:, ivar]-x[:, ivar].mean()) / x[:, ivar].std()
        c[ivar, :] = scs.correlate(xtemp, xtemp, "full")/N
        # plot power spectrum
        f, Pxx[ivar, :] = scs.welch(xtemp, fs=1, nperseg=nps,
                                    scaling='density')

    Pxx = Pxx / N

    if Nvar > 0:
        c = c.transpose()
        Pxx = Pxx.transpose()

    return c, cx, Pxx, f


# %%
def synthmrw(dim, N, H, lambda2, rho=None, rhomf=None, Ani=[1, 0],
             workers=1, validate=False):
    """
    Generate 1 dimensional or 2 dimensional
             Multivariate  Multifractal Random field.

    The scaling can be monofractal (lambda2=0) or
    multifractal (sigma2>0). The exponent of the structure functions are

        zeta(q)=q*(H+lambda2/2) -q**2*lambda2/2

    The case lambda2 = 0 correspond to Fractional Brownian Fields.


    MRW, MRWnise synthmrw(dim, N, H, lambda2, rho=None, rhomf=None, Ani=[1, 0],
                          workers=1, validate=0):

   Parameters
   ----------
   dim: int.
       dimension of the process to be generated.
   N : int
       size of the process.
   H : array of float of size (Nvar, )
       Contains the Hurst exponent (0<H<1) of each signal to be generated.
   lambda2 : array of float of size (Nvar, )
       Contains the intermittency parameter of each signal to be generated.
       Must have the same length as H. The default is 0.025.
   rho : array of float, of size (Nvar, Nvar) optional
       For multivariate synthesis only.
       Contains the correlation matrix of the noise used in the generation
       of the FGN. The default is None.
   rhomf : array of float, of size (Nvar, Nvar), optional
       rray of float, optional
           For multivariate synthesis only.
           Contains the correlation matrix of the noise used in the generation
           of the multiplicative chaos. The default is None.
   Ani : list of size 2 or 4, optional
       Anisotropic parameters  used if dim =2
       Ani=[alphaSS, thetaSS] where
          0 < alphaSS < 2. For alphaSS != 1,
              the space is dilated differently for x and y direction)
          0 < thetaSS < 2*pi. Angle of the generating base.
       The deformation of the space are the same for the two noises.
       if Ani list of size 4 the anisotropic parameters for the two noises.
       Ani=[alphaSS, thetaSS, alphaMF, thetaMF]
       The default is Ani =[1, 0].
   workers : int, optional.
       Number of threads to use in the FFT's. The default is 1.
   validate : int, optional
       Contains the number of signal to analyse. If superior to 0 the
       function display an analysis of the synthetized signals.
       The default is 0.

   Returns
   -------
   MRW : array of float of size(N,Nvar) or (N,N, Nvar)
       The generated process(es).
   MRWnoise: array of float of size(N,Nvar) or (N,N, Nvar)
       The noise of the generated process(es).

   Raises
   ------
   TypeError
       if the input parameters are not well choosen.

   References
   ----------
   For one dimensional processes
       E. Bacry, J. Delour, and J.-F. Muzy.
       Multifractal random walk.
       Physical Review E, 64(2):026103, 2001.

   For two dimensional processes (monofractal)
       A. Bonami and A. Estrade,
       Anisotropic analysis of some gaussian models,
       The Journal of Fourier Analysis and Applications,
       vol. 9, no. 3, pp. 215–236, 2003.

       H. Bierme, M. Meerschaert, and H. Scheffler,
       Operator scaling stable random fields.
       Stoch. Proc. Appl., vol. 117, no. 3, pp. 312–332, 2009

   Examples
   --------
   create one realisation of an isotropic field
   >>> dim = 1
   >>> N = 2**10
   >>> H = 0.7
   >>> c2 = 0.025
   >>> MRW = synthMultiFractalField(dim, 2**16, H, c2)

   create two independant realisation
   >>> rho=np.array([[1,0],[0,1]])
   >>> rhomf=np.array([[1,0],[0,1]])
   >>> MRW = synthMultiFractalField(dim, N, [H, H], [c2, c2],
                                    rho=rho, rhomf=rhomf)

   create two dependant realisations of an isotropic field
   >>> rho=np.array([[1,.2],[.2,1]])
   >>> rhomf=np.array([[1,-.4],[-.4,1]])
   >>> MRW = synthMultiFractalField(dim, N, [H, H], [c2, c2],
                                    rho=rho, rhomf=rhomf)

   create two dependant realisations of an anisotropic field
   >>> rho=np.array([[1,.2],[.2,1]])
   >>> rhomf=np.array([[1,-.4],[-.4,1]])
   >>> Ani=[.8, 0]
   >>> MRW = synthMultiFractalField(dim, N, [H, H], [c2, c2],
                                    rho=rho, rhomf=rhomf, Ani=Ani)

   create field and display analysis of Nreal realisation
   >>> Nreal =5
   >>> MRW = synthMultiFractalField(dim, N, [H, H], [c2, c2],
                                    rho=rho, rhomf=rhomf, Ani=Ani,
                                    validate =  Nreal)

   ##
   S.G.  Roux, ENS Lyon, May 2021

   """

    # check parameters
    params = checkparams(dim, N, H, lambda2, rho, rhomf, Ani)
    if params["dim"] == 0:
        raise TypeError('Wrong input parameters.\n')

    if validate > 0:
        MRW, MRWnoise = validatesynthesis(synthmrw, dim, N, H, lambda2,
                                          rho, rhomf, Ani, Nreal=validate,
                                          workers=workers)
        return MRW, MRWnoise

    # large scale for multiplicative chaos
    Nvar = params['Nvar']
    s = 1  # variance
    L = 1/2
    T = L*N

    # Sampling space
    timeper = np.zeros(N)
    timeper[0:int(N/2)+1] = np.arange(0, int(N/2)+1)
    timeper[int(N/2)+1:2*N] = np.arange(-int(N/2)+1, 0)

    # ###################################### 1D case
    if params['dim'] == 1:  # signal
        # ##############################
        # First step : fgn synthesis
        # ##############################
        # creation of the correlation
        L2 = np.zeros((Nvar, N))+1j * np.zeros((Nvar, N))
        for ivar in range(Nvar):
            currH = params['H'][ivar]
            r = s/2 * (np.abs(timeper - 1)**(2*currH) +
                       np.abs(timeper + 1)**(2*currH) -
                       2*np.abs(timeper)**(2*currH))
            L2[ivar, :] = fft(r, axes=0, workers=workers)

        # synthesis of the correlated noises
        if Nvar > 1:
            w = np.dot(np.random.randn(N, Nvar), params['Upp'])
        else:
            w = np.random.randn(N, Nvar)

        w2 = np.sqrt(L2.transpose())*fft(w, axes=0, workers=workers)
        z = ifft(w2, axes=0, workers=workers)
        x1 = z[:N, :].real
        del L2, w2, z

        # ##############################
        # second step : multiplicaticve chaos synthesis
        # ##############################
        # creation of the correlation
        timepertmp = timeper
        timepertmp[np.abs(timepertmp) > T] = T
        lacorr1d = np.log(T/(np.abs(timepertmp)+1))
        lacorr1d[lacorr1d < 0] = 0
        # TF of it
        phi = np.zeros((Nvar, N))+1j * np.zeros((Nvar, N))
        for ivar in range(Nvar):
            currlambda2 = params['lambda2'][ivar]
            phi[ivar, :] = fft(currlambda2*lacorr1d, axes=0, workers=workers)

        # synthesis of the correlated noises
        if Nvar > 1:
            wtemp = np.dot(np.random.randn(N, Nvar), params['Uppmf'])
        else:
            wtemp = np.random.randn(N, Nvar)

        # multiplicative chaos
        w = fft(wtemp, axes=0, workers=workers)
        x2 = np.real(ifft(w*np.sqrt(phi.transpose()), axes=0, workers=workers))

        # MRW noise = fgn * exp(x2)
        MRWnoise = x1*np.exp(x2)/np.sqrt(2)
        MRW = np.cumsum(MRWnoise, axis=0)-np.tile(MRWnoise[0, None], (N, 1))

    elif params['dim'] == 2:  # ################################# 2D case
        # ##############################
        # first step : fgn synthesis
        # ##############################

        # space between -1 and 1
        timeperbis = timeper/N*2
        [X, Y] = np.meshgrid(timeperbis, timeperbis)

        # rotate the 2D space
        if params['thetaSS'] > 0:
            X1 = np.cos(params['thetaSS'])*X-np.sin(params['thetaSS'])*Y
            Y1 = np.sin(params['thetaSS'])*X+np.cos(params['thetaSS'])*Y
        else:
            X1 = X
            Y1 = Y

        # dilate the space
        r = (np.abs(X1)**(2/params['alphaSS']) +
             np.abs(Y1)**(2/(2-params['alphaSS'])))

        # multivariate correlation of fbm
        L2 = np.zeros((Nvar, N, N))+1j * np.zeros((Nvar, N, N))

        for ivar in range(Nvar):
            currH = params['H'][ivar]
            a = 2*currH
            P = 1-1/2*a-r**currH+1/2*a*r
            P[r >= 1] = 0
            L2[ivar, :, :] = fft(P, axes=(0, 1), workers=workers)

        # transpose
        L2 = np.transpose(L2, (1, 2, 0))
        # generation of the correlated noises
        wtemp = np.random.randn(N, N, Nvar)
        if Nvar > 1:
            w = np.dot(wtemp.reshape(N**2, Nvar), params['Upp'])
            ww = w.reshape(N, N, Nvar)
        else:
            ww = wtemp

        # take TF and multiply by FBM correlation
        x = fft(ww, axes=(0, 1), workers=workers)
        y = np.sqrt(2)*ifft(x*np.sqrt(L2), axes=(0, 1), workers=workers)
        B = y.real  # the FBM

        # compute assiciate fgn : derivation in fourier space
        r[r == 0] = np.min(r[r > 0])
        Hfilt = 1./(r**(-1/2))  # 1/2 car r n'a pas la sqrt

        X = fft(B, axes=(0, 1), workers=workers)
        Hfilt = np.tile(Hfilt, (Nvar, 1, 1))
        Hfilt = np.transpose(Hfilt, (1, 2, 0))
        fgn = ifft(Hfilt*X, axes=(0, 1), workers=workers).real  # the fgn

        # normalisation of the std of fgn
        lastd = np.std(fgn.std(axis=0), axis=0)
        noise = fgn/np.tile(lastd, (N, N, 1))

        # #############################
        # second step : multiplicaticve chaos synthesis
        # #############################

        # define the correlation and take the fft of it
        timepertmp = np.abs(timeper)
        timepertmp[np.abs(timepertmp) > T] = T
        [X, Y] = np.meshgrid(timepertmp, timepertmp)
        # rotate the 2D space
        if np.abs(params['thetaMF']) > 0:
            X2 = np.cos(params['thetaMF'])*X-np.sin(params['thetaMF'])*Y
            Y2 = np.sin(params['thetaMF'])*X+np.cos(params['thetaMF'])*Y
        else:
            X2 = X
            Y2 = Y

        # dilation
        R = np.sqrt(np.abs(X2)**(2/params['alphaMF']) +
                    np.abs(Y2)**(2/(2-params['alphaMF'])))
        # define the correlation in log
        lacorr2d = np.log(T/(np.abs(R)+1))
        lacorr2d[lacorr2d < 0] = 0
        # take the fft of it for each lambda2
        phi2var = np.zeros((Nvar, N, N))+1j*np.zeros((Nvar, N, N))
        for ivar in range(Nvar):
            currlambda2 = params['lambda2'][ivar]
            phi2var[ivar, :, :] = currlambda2*fft(lacorr2d, axes=(0, 1),
                                                  workers=workers)

        # transpose results
        phi2var = np.transpose(phi2var, (1, 2, 0))
        # generation of the correlated noise and take the fft
        wtemp = np.random.randn(N, N, Nvar)
        if Nvar > 1:
            w = np.dot(wtemp.reshape(N**2, Nvar), params['Uppmf'])
            ww = w.reshape(N, N, Nvar)
        else:
            ww = wtemp

        # MRW noise  = fgn * exp(X2)
        frndg1 = fft(ww, axes=(0, 1), workers=workers)
        # Xr = np.exp(np.real(
        #        ifft(frndg1*np.sqrt(phi2var), axes=(0, 1), workers=workers)))
        for ivar in range(Nvar):
            if params['lambda2'][ivar] == 0:
                frndg1[:, :, ivar] = 1

        MRWnoise = noise*np.exp(np.real(
            ifft(frndg1*np.sqrt(phi2var), axes=(0, 1), workers=workers)))
        MRWnoise = MRWnoise/np.sqrt(2)/np.sqrt(N)

        # ----> we integrate by 1 :
        # space : no dilation, no rotation : using X instead of X2
        r = np.sqrt(np.abs(X)**2+np.abs(Y)**2)
        r[r == 0] = np.min(r[r > 0])
        # create the filter
        Hfiltint1 = 1./(r**(1))
        Hfiltint1b = np.tile(Hfiltint1, (Nvar, 1, 1))
        Hfiltint1b = np.transpose(Hfiltint1b, (1, 2, 0))
        # integrate
        X = fft(MRWnoise, axes=(0, 1), workers=workers)
        MRW = ifft(Hfiltint1b*X, axes=(0, 1), workers=workers).real
        MRW = MRW*np.sqrt(N)/2

    else:

        print('Not yet implemented')
        MRW = []
        MRWnoise = []

    return MRW, MRWnoise


# %% Plot functions for validation
def plot_Structure_Functions1d(Mom2, Mom4, scales, ax1=None, ax2=None,
                               label='process'):
    """
    plot second order structure function and the flatness versus scales

    axM, axF = plot_Structure_Functions1d(Mom2, Mom4, scales,
                                          ax1=None, ax2=None,
                                          label='process'):

    Parameters
    ----------
    Mom2 : array of size (Nvar, Nscales)
        Moment of order 2.
    Mom4 : array of size (Nvar, Nscales)
        Moment of order 4.
    scales : array of size (Nscales, )
        scales used for the computation of the moments.
    ax1 : Axes, optional
        The Axes of the subplo of the structure function. The default is None.
    ax2 : Axes, optional
        The Axes of the subplo of the flatness. The default is None.
    label : TYPE, optional
        DESCRIPTION. The default is 'process'.

    Returns
    -------
    axM : Axes
        The Axes of the subplo of the structure function.
    axF : Axes
        The Axes of the subplo of the flatness.

    ##
     S.G.  Roux, ENS Lyon, May 2021
     C. Granero-Belinchon, IMT atlantique, May 2021

    """

    # compute mean and std
    if len(Mom2.shape) > 1 and Mom2.shape[0] > 1:
        # Estimation of mean and std of the estimates
        lmMom2 = np.mean(np.log(Mom2), axis=0)
        lsMom2 = np.std(np.log(Mom2), axis=0)
        lmFlat = np.mean(np.log(Mom4/(3*Mom2**2)), axis=0)
        lsFlat = np.std(np.log(Mom4/(3*Mom2**2)), axis=0)
    else:
        # Estimation of mean and std of the estimates
        Mom2 = Mom2.reshape(len(scales,))
        Mom4 = Mom4.reshape(len(scales,))
        lmMom2 = np.log(Mom2)
        lsMom2 = 0.0*lmMom2
        lmFlat = np.log(Mom4/(3*Mom2**2))
        lsFlat = 0.0*lmFlat

    if ax1 is None:
        fig, (ax1, ax2) = plt.subplots(2, 1, clear=True, figsize=(6, 6))

    # Second order structure function plot
    ax1.errorbar(np.log(scales), lmMom2, yerr=lsMom2, color='k', label=label)
    P = np.polyfit(np.log(scales[1:]), lmMom2[1:], 1)
    ax1.plot(np.log(scales), np.polyval(P, np.log(scales)), color='b',
             linestyle='dashed', label="fit : slope={:.2f}".format(P[0]))

    leg = ax1.legend()
    for line, text in zip(leg.get_lines(), leg.get_texts()):
        text.set_color(line.get_color())
    ax1.set_ylabel(r'$\log(M_2(\tau))$', fontsize=15)
    plt.tight_layout()

    # Flatness plot
    ax2.errorbar(np.log(scales), lmFlat, yerr=lsFlat, color='k', label=label)
    P = np.polyfit(np.log(scales[1:]), lmFlat[1:], 1)
    ax2.plot(np.log(scales), P[0]*np.log(scales)+P[1], color='b',
             linestyle='dashed', label="fit : slope={:.2f}".format(P[0]))

    leg = ax2.legend()
    for line, text in zip(leg.get_lines(), leg.get_texts()):
        text.set_color(line.get_color())
    ax2.set_xlabel(r'$\log(\tau)$', fontsize=15)
    ax2.set_ylabel(r'$\log(Flatness)$', fontsize=15)
    plt.tight_layout()

    return ax1, ax2


# %% plot_validation
def plot_correlation_spectrum1d(c, cx, Pxx, f, ax1=None, ax2=None,
                                label='Noise'):
    '''
    Plot autocorrelation correlationand spectrum

    axC, axP = plot_correlation_spectrum1d(c, cx, Pxx, f,
                                           ax1=None, ax2=None,
                                           label='Noise')

    Inputs:

        c   : Correlation function. It should be an array of shape
             (N_Realizations, scales) or (scales)
        cx  : Array of scales. 1d array
        Pxx : Power spectrum. It should be an array of shape
              (N_Realizations, scales) or (scales)
        f   : Array of frequencies. 1d array
        T   : Large scale defining the region of fit.
        epsilon: small scale defining the region of fit.

    Parameters
    ----------
    c : array of size (Nvar, Nscales)
        Correlation function.
    cx : array of size (Nvar, Nscales)
        Lags corresponding of the correlation function
    Pxx : array of size (Nvar, Nscales)
        power spectrum
    f : array of size (Nvar, Nscales)
        Frequencies of the power spectrum
    ax1 : Axes, optional
        The Axes of the subplo of the autocorrelation function.
        The default is None.
    ax2 : Axes, optional
        The Axes of the subplo of the power spectrum. The default is None.
    label : TYPE, optional
        DESCRIPTION. The default is 'process'.

    Returns
    -------
    axC : Axes
        The Axes of the subplo of the structure function.
    axP : Axes
        The Axes of the subplo of the flatness.

    ##
     S.G.  Roux, ENS Lyon, May 2021
     C. Granero-Belinchon, IMT atlantique, May 2021
     '''

    # We define the scales of correlation
    N = (c.shape[1]+1)/2

    lcx = cx*0
    lcx = np.log(np.abs(cx[cx != 0]/N))
    cx = cx[1:-1]
    lcx = lcx[1:-1]

    # Correlation normalization
    c = c[:, 1:-1]

    # We define the scales for spectrum
    lf = np.log(f[f != 0])

    # compute mean and std
    if len(c.shape) > 1 and c.shape[0] > 1:
        # Estimation mean and std of log of corr
        lmc = np.mean(np.log(np.abs(c[:, cx != 0])), axis=0)
        lsc = np.std(np.log(np.abs(c[:, cx != 0])), axis=0)
        # We define the mean and std of log of spextrum
        lmPxx = np.mean(np.log(Pxx[:, f != 0]), axis=0)
        lsPxx = np.std(np.log(Pxx[:, f != 0]), axis=0)
    else:
        # Estimation of mean and std of the estimates
        c = c.reshape(len(cx),)
        lmc = np.log(np.abs(c[cx != 0]))
        lsc = 0.0*lmc
        # We define the mean and std of log of spextrum
        Pxx = Pxx.reshape(len(f),)
        lmPxx = np.log(Pxx[f != 0])
        lsPxx = 0.0*lmPxx

    if ax1 is None:
        fig, (ax1, ax2) = plt.subplots(2, 1, clear=True, figsize=(6, 6))

    P = np.polyfit(lcx[1:], lmc[1:], 1)

    # Plot Power Spectrum
    ax2.errorbar(lf, lmPxx, yerr=lsPxx, color='k', label=label)
    PP = np.polyfit(lf[1:], lmPxx[1:], 1)

    ax2.plot(lf, PP[0]*lf+PP[1], color='b', linestyle='dashed',
             label="fit : slope={:.2f}".format(PP[0]))

    leg = ax2.legend(loc='upper right')
    for line, text in zip(leg.get_lines(), leg.get_texts()):
        text.set_color(line.get_color())
    ax2.set_xlabel(r'$\log(\frac{1}{\tau})$', fontsize=15)
    ax2.set_ylabel(r'$\log(P.S.D)$', fontsize=15)

    ax1.errorbar(lcx[::], lmc[::], yerr=lsc[::], color='k', label=label)
    tmp = np.array([lcx[0], lcx[1]])
    ax1.plot(tmp, P[0]*tmp+P[1], color='b', linestyle='dashed',
             label="fit : slope={:.2f}".format(P[0]))

    # color text of legend following color symbol
    leg = ax1.legend(loc='upper right')
    for line, text in zip(leg.get_lines(), leg.get_texts()):
        text.set_color(line.get_color())
    ax1.set_xlabel(r'$\log(\tau)$', fontsize=15)
    ax1.set_ylabel(r'$\log(R(\tau))$', fontsize=15)

    plt.tight_layout()

    return ax1, ax2, P, PP


# %% validation synthesis
def validatesynthesis(func, dim, N, H, lambda2, rho, rhomf, Ani,
                      regparam=0, workers=1, Nreal=3):
    """
    Validation of the synthesis function. Plot structure function of order 2,
    flatness, power spectrum and autocorrelation function. The two later ae
    computed on the noise or the process if it is stationnary.

    MRW, MRWnoise, validatesynthesis(func, dim, N, H, lambda2,
                                     rho, rhomf, Ani,
                                     regparam=0, workers=1, Nreal=1)

     Parameters
     ----------
     func : function
       DESCRIPTION.
     dim: int.
       dimension of the process to be generated.
     N : int
       size of the process.
     H : array of float
       Contains the Hurst exponent (0<H<1) of each signal to be generated.
       lambda2 : float
       Contains the intermittency parameter of each signal to be generated.
       Must have the same length as H. The default is 0.025.
     rho : array of float, optional
        For multivariate synthesis only.
        Contains the correlation matrix of the noise used in the generation
        of the FGN. The default is None.
     rhomf : array of float, optional
        For multivariate synthesis only.
        Contains the correlation matrix of the noise used in the generation
        of the multiplicative chaos. The default is None.
     Ani : float, optional
        Ani=[alpha, theta]
        Contains anisotropic coefficient (0 < alpha <2).
        For anisotropic process \alpha=1.
     theta: list of size 2 por 4, optional
         Anisotropic parameters  used if dim =2Optional
         Ani=[alphaSS, thetaSS] where
            0 < alphaSS < 2. For alphaSS != 1,
                the space is dilated differently for x and y direction)
            0 < thetaSS < 2*pi. Angle of the generating base.
         The deformation of the space are the same for the two noises.
         if Ani list of size 4 the anisotropic parameters for the two noises.
         Ani=[alphaSS, thetaSS, alphaMF, thetaMF]
         The default is Ani =[1, 0].
     regparam : list of size 2, optional
        regularisation parameters regparam=[epsilon, T] where epsilon is the
        disspation scale and T is the integral scale.
        1/2 < epsilon < T <N
        The default is [2, N//4].
     workers : int, optional.
        Number of threads to use in the FFT's and the structure function.
        The default is 1.
     Nreal : int, optional
        Contains the number of realizations of the process to analyse.
        The default is 3.

    Raises
    ------
    TypeError
        if the input parameters are not well choosen.

    Returns
    -------
    MRW : array of float of size(N,Nvar) or (N,N, Nvar)
        The generated process(es).
    MRWnoise: array of float of size(N,Nvar) or (N,N, Nvar)
        The noise of the generated process(es).
        if the process is stationary, i.e. using func=synthMFAU or
        func=synthMultiFractalField, MRWnoise= MRW.

    ##
    S.G.  Roux, ENS Lyon, May 2021

    """

    nv = 5
    nps = np.maximum(N//8, 2**12)  # Size of patches in pwelch/corr method
    # check parameters
    params = checkparams(dim, N, H, lambda2, rho, rhomf, Ani)
    if params['dim'] == 0:
        raise TypeError('Wrong input parameters.\n')

    # which function to validate
    isnoise = True
    label = 'Noise'
    isFou = False
    if func != synthmrw:  # test regparam
        isnoise = False
        label = 'process'
        test = True
        regparam = np.atleast_1d(regparam)
        if regparam[0] == 0:
            regparam = [2, N//4]
        elif regparam[0] > regparam[1] and regparam[1] < 1:
            test = False
            print('epsilon must be an integer between 1 and T.\n')
        else:
            regparam[0] = max(regparam[0], 0.5)
            regparam[1] = int(regparam[1])
        if not test:
            raise TypeError('Wrong input parameters.\n')
        if func == synthMFOU:
            isFou = True
            print('running synthMFOU function')
            zeta2 = 2*params['H']
            flat = -4*params['lambda2']
            ptheo = -(2*params['H']+1)
            ctheo = None
            func = partial(func, regparam=regparam)
        else:
            print('running synthMultiFractalField function')
            zeta2 = 2*params['H'] - params['lambda2']
            flat = -4*params['lambda2']
            ptheo = -(2*params['H'] - params['lambda2']+1)
            ctheo = None
            func = partial(func, regparam=regparam, rho=rho, rhomf=rhomf,
                           Ani=Ani)
    else:
        print('running synthmrw function')
        zeta2 = 2*params['H'] - params['lambda2']
        flat = -4*params['lambda2']
        ctheo = (2*(params['H'] - 1) - params['lambda2'])
        ptheo = -1*np.abs(2*params['H'] - params['lambda2']-1)
        func = partial(func, rho=rho, rhomf=rhomf, Ani=Ani)
    #
    if nps > N:
        nps = int(N/4)
    #
    nps = int(N/4)
    Nvar = params['Nvar']
    Nreal = int(Nreal)

    # Initialization of Matrix
    cor = np.zeros((Nreal, 2*N-1, Nvar))

    uu = 2**np.arange(0, np.log2(N)-3, 1/nv)
    scales = np.unique(uu.astype(int))
    if dim == 1:
        Pxx = np.zeros((Nreal, int(nps/2+1), Nvar))
        Mom2 = np.zeros((Nreal, len(scales), Nvar))
        Mom4 = np.zeros((Nreal, len(scales), Nvar))
    else:
        Pxx = np.zeros((Nreal, int(N/2), Nvar, 3))
        cor = np.zeros((Nreal, int(N/2), Nvar, 3))
        Mom2 = np.zeros((Nreal, len(scales), Nvar, 3))
        Mom4 = np.zeros((Nreal, len(scales), Nvar, 3))
    print('Validation on {:d} realisations.'.format(Nreal))

    for ireal in range(Nreal):
        print(ireal, end=' ')
        # synthesis
        if isnoise:
            MRW, MRWnoise = func(dim, N, H, lambda2, workers=workers)
        else:
            if isFou:
                MRW = func(N, H, lambda2, workers=workers)
            else:
                MRW = func(dim, N, H, lambda2, rho=rho, rhomf=rhomf, Ani=Ani,
                           workers=workers)
            MRWnoise = MRW

        # analysis
        if dim == 1:
            # moments
            Mom2[ireal, :,
                 :], Mom4[ireal, :,
                          :], scales = analyseIncrSimple(MRW,
                                                         nv=nv,
                                                         workers=workers)
            # corr/spectrum
            cor[ireal, :,
                :], cx, Pxx[ireal, :,
                            :], f = analyse1dCorrSpectrum(MRWnoise, nps=nps)

        else:
            # moments
            for ivar in range(Nvar):
                Mom2[ireal, :, ivar,
                     :], Mom4[ireal, :, ivar,
                              :], scales = analyseIncrSimple(MRW[:, :, ivar],
                                                             nv=nv,
                                                             dim=2,
                                                             workers=workers)
            # spectre and auto correlation
            uu = np.abs(fft(MRWnoise, axes=(0, 1), workers=workers))
            lacor = ifft(abs(
                fft(MRWnoise, axes=(0, 1), workers=workers))**2,
                axes=(0, 1), workers=workers).real/N/N
            for ivar in range(Nvar):
                # Pxx[ireal,:,ivar,0]=np.diag(uu[:,0:int(N/2),ivar])
                Pxx[ireal, :, ivar, 0] = np.diag(uu[:, 0:int(N/2), ivar])
                Pxx[ireal, :, ivar, 1] = uu[0, 0:int(N/2), ivar]
                Pxx[ireal, :, ivar, 2] = uu[0:int(N/2), 0, ivar]
                cor[ireal, :, ivar, 0] = np.diag(lacor[:, 0:int(N/2), ivar])
                cor[ireal, :, ivar, 1] = lacor[0, 0:int(N/2), ivar]
                cor[ireal, :, ivar, 2] = lacor[0:int(N/2), 0, ivar]

    if (dim == 1) and (N > 2**15):
        cor = cor[:, ::N//2**14]
        cx = cx[::N//2**14]

    # display signal
    plt.figure(11)
    plt.clf()
    for ivar in range(Nvar):
        if dim == 1:
            plt.subplot(np.maximum(Nvar//2, 1), 2, ivar+1)
            temp = MRW[:, ivar]/np.std(MRW[:, ivar])
            plt.plot(MRW[0:np.minimum(N, 2**14), ivar], 'k')
            plt.title('H={:.2f}, $c_2$={:.3f}'.format(params['H'][ivar],
                                                      params['lambda2'][ivar]))
        else:
            plt.subplot(np.maximum(Nvar//2, 1), 2, ivar+1)
            plt.imshow(MRW[:, :, ivar])
            plt.title('H={:.2f}, $c_2$={:.3f}'.format(params['H'][ivar],
                                                      params['lambda2'][ivar]))

    lflat = np.log(np.mean(Mom4/Mom2**2/3, axis=0))
    lM2 = np.log(np.mean(Mom2, axis=0))
    if dim == 1:
        lcx = np.log(np.abs(cx[cx != 0]/N))
        for ivar in range(Nvar):

            fig, ax = plt.subplots(2, 2, num=ivar, clear=True, figsize=(6, 6))

            fig.suptitle('Componant  {:d}'.format(ivar+1))
            axM, axF = plot_Structure_Functions1d(Mom2[:, :, ivar],
                                                  Mom4[:, :, ivar],
                                                  scales,
                                                  ax1=ax[0, 0],
                                                  ax2=ax[0, 1])
            axM.title.set_text('Moment of order 2 of increments')

            intercept = np.mean(np.log(Mom2[:, 0, ivar]))
            axM.plot(np.log(scales), zeta2[ivar]*np.log(scales)+intercept,
                     color='r', linestyle='dashed',
                     label='Theory slope={:.2f}'.format(zeta2[ivar]))
            P = np.polyfit(np.log(scales), lflat[:, ivar], 1)
            axF.plot(np.log(scales), flat[ivar]*np.log(scales)+P[1],
                     color='r', linestyle='dashed',
                     label='Theory slope={:.2f}'.format(-flat[ivar]))
            axF.title.set_text('Flatness of increments')
            axM.legend()
            axF.legend()

            axC, axP, P, PP = plot_correlation_spectrum1d(cor[:, :, ivar],
                                                          cx,
                                                          Pxx[:, :, ivar],
                                                          f,
                                                          ax1=ax[1, 0],
                                                          ax2=ax[1, 1],
                                                          label=label)

            if ctheo is not None:
                if isnoise:
                    intercept = P[0]*np.min(lcx)+P[1]-dim*ctheo[ivar]*np.min(lcx)
                    tmp = np.array([np.min(lcx[::2]), np.argmax(lcx[::2])])
                    axC.plot(tmp, ctheo[ivar]*tmp+intercept, color='r',
                             linestyle='dashed',
                             label='Theory slope={:.2f}'.format(dim*ctheo[ivar]))

                else:
                    intercept = P[0]*np.min(lcx)+P[1]-ctheo[ivar]*np.min(lcx)
                    tmp = np.array([np.min(lcx[::2]), np.argmax(lcx[::2])])
                    axC.plot(tmp, ctheo[ivar]*tmp+intercept, color='r',
                             linestyle='dashed',
                             label='Theory slope={:.2f}'.format(ctheo[ivar]))

            axP.plot(np.log(f[1:]),
                     ptheo[ivar] * np.log(f[1:])+PP[1],
                     color='r', linestyle='dashed',
                     label='Theory slope={:.2f}'.format(ptheo[ivar]))

            axC.title.set_text('auto correlation of the ' + label)
            axC.legend(loc='lower left')
            axP.title.set_text('Power spectrum of the ' + label)
            axP.legend(loc='lower left')
            fig.suptitle('Componant  {:d}'.format(ivar+1))
            plt.tight_layout(rect=[0, 0.03, 1, 0.95])

    else:
        for ivar in range(Nvar):

            fig, ax = plt.subplots(2, 2, num=ivar, clear=True, figsize=(6, 6))
            axP = ax[1, 1]
            axC = ax[1, 0]
            fig.suptitle('Componant  {:d}'.format(ivar+1))

            axM, axF = plot_Structure_Functions1d(Mom2[:, :, ivar, 0],
                                                  Mom4[:, :, ivar, 0],
                                                  scales, ax1=ax[0, 0],
                                                  ax2=ax[0, 1],
                                                  label='diag')
            axM.plot(np.log(scales), lM2[:, ivar, 1], 'b.', label='x')
            axM.plot(np.log(scales), lM2[:, ivar, 2], 'b-', label='y')
            axM.title.set_text('Moment of order 2 of increments')

            intercept = np.mean(np.log(Mom2[:, 0, ivar]))
            axM.plot(np.log(scales), zeta2[ivar]*np.log(scales)+intercept+1.5,
                     color='r', linestyle='dashed',
                     label='Theory slope={:.2f}'.format(zeta2[ivar]))

            P = np.polyfit(np.log(scales), lflat[:, ivar, 0], 1)

            axF.plot(np.log(scales), lflat[:, ivar, 1], 'b.', label='x')
            axF.plot(np.log(scales), lflat[:, ivar, 2], 'b', label='y')
            axF.plot(np.log(scales), flat[ivar]*np.log(scales)+P[1],
                     color='r', linestyle='dashed',
                     label='Theory slope={:.2f}'.format(flat[ivar]))

            axM.legend()
            axF.legend()
            axF.title.set_text('Flatness of increments')

            f = np.arange(0, N/2)/N
            with np.errstate(divide='ignore'):
                lPx = np.mean(np.log(Pxx), axis=0)
                lcx = np.mean(np.log(np.abs(cor)), axis=0)

            axP.plot(np.log(f[1:]), lPx[1:, ivar, 0], 'k', label=label)
            axC.plot(np.log(f[1:]), lcx[1:, ivar, 0], 'k', label=label)

            PP = np.polyfit(np.log(f[1:]), lPx[1:, ivar, 0], 1)
            axP.plot(np.log(f[1:]), np.log(f[1:])*PP[0]+PP[1], 'c',
                     label='fit{:.2f}'.format(PP[0]))
            axP.plot(np.log(f[1:]), lPx[1:, ivar, 1], 'b', label='x')
            axP.plot(np.log(f[1:]), lPx[1:, ivar, 2], 'b--', label='y')
            axP.plot(np.log(f[1:]), ptheo[ivar]*np.log(f[1:])+PP[1],
                     color='r', linestyle='dashed',
                     label='Theory slope={:.2f}'.format(ptheo[ivar]))

            PP2 = np.polyfit(np.log(f[1:]), lcx[1:, ivar, 0], 1)
            axC.plot(np.log(f[1:]), np.log(f[1:])*PP2[0]+PP2[1], 'c',
                     label='fit{:.2f}'.format(PP2[0]))
            axC.plot(np.log(f[1:]), lcx[1:, ivar, 1], 'b', label='x')
            axC.plot(np.log(f[1:]), lcx[1:, ivar, 2], 'b--', label='y')
            if ctheo is not None:
                axC.plot(np.log(f[1:]), ctheo[ivar]*np.log(f[1:])+PP2[1],
                         color='r', linestyle='dashed',
                         label='Theory slope={:.2f}'.format(ctheo[ivar]))

            axP.title.set_text('Power spectrum of the ' + label)
            axP.set_xlabel(r'$\log(\frac{1}{\tau})$', fontsize=15)
            axP.set_ylabel(r'$\log(P.S.D)$', fontsize=15)
            axP.legend(loc='lower left')

            axC.title.set_text('auto correlation of the ' + label)
            axC.set_xlabel(r'$\log(\tau)$', fontsize=15)
            axC.set_ylabel(r'$\log(R(\tau))$', fontsize=15)
            axC.legend(loc='lower left')

            fig.suptitle('Componant  {:d}'.format(ivar+1))
            plt.tight_layout(rect=[0, 0.03, 1, 0.95])

    return MRW, MRWnoise


# %%
def synthMFOU(N, H, lambda2, regparam=0, rho=None, rhomf=None,
              workers=1, validate=0):
    """

    Synthesis of a 1d causal Multivariate Multifractal fractionary Oulenbeck process.
    The scaling can be monofractal (lambda2=0) or
    multifractal (sigma2>0). The exponent of the structure functions are

        zeta(q)=q*H-q(*q-1)*lambda2

    For scale below epsilon, there is no intermittency
    and the scaling exponent is 1.
    For scale larger than T, there is a constant behavior.


    MRW = synthMFOU(N, H, lambda2, rho=None, rhomf=None, Ani=[1, 0],
                    regparam=0, workers=1, validate=0)

    Parameters
    ----------
     N : int
       size of the process.
     H : array of float of size (Nvar,)
       Contains the Hurst exponent (0<H<1) of each signal to be generated.
     lambda2 : array of float of size (Nvar,)
       Contains the intermittency parameter of each signal to be generated.
       Must have the same length as H. The default is 0.025.
     regparam : list of size 2, optional
        regularisation parameters regparam=[epsilon, T] where epsilon is the
        disspation scale and T is the integral scale.
        1/2 < epsilon < T <N
        The default is [2, N//4].
     workers : int, optional
        Number of threads to use in the FFT's. The default is 1.
     validate : int, optional
        Contains the number of signal to analyse. If superior to 0 the
        function display an analysis of the generated signals.

    Raises
    ------
    TypeError
        if the input parameters are not well choosen.

    References
    ----------
    For the multifractal case :

        L. Chevillard, M. Lagoin, S.G. Roux,
        Multifractal Fractional Ornstein-Uhlenbeck Processes.
        arXiv:2011.09503 (2020).

    For the monofractal case :

        L. Chevillard,
        Regularized fractional Ornstein-Uhlenbeck processes,
        and their relevance to the modeling of fluid turbulence.
        Phys. Rev. E 96, 033111 (2017).

    Returns
    -------
    MRW : array of float of size(N,Nvar) or (N,N, Nvar)
        The generated process(es).

    ##
    S.G.  Roux, ENS Lyon, May 2021

    """
    dim = 1
    rho = None
    rhomf = None
    Ani = [1, 0]
    regparam = np.atleast_1d(regparam)

    # check regparam
    test = True
    if regparam[0] == 0:
        epsilon = .5
        T = N/4
    elif regparam[0] > regparam[1] and regparam[1] < 1:
        test = False
        print('epsilon must be an integer between 1 and T.\n')
    else:
        epsilon = max(regparam[0], 0.5)
        T = int(regparam[1])
    if not test:
        raise TypeError('Wrong input parameters.\n')

    # check parameters
    params = checkparams(dim, N, H, lambda2, rho, rhomf, Ani)
    if params['dim'] == 0:
        raise TypeError('Wrong input parameters.\n')

    if validate > 0:
        # print('toto')
        MRW, MRWnoise = validatesynthesis(synthMFOU,
                                          dim, N, H, lambda2,
                                          rho=rho, rhomf=rhomf, Ani=Ani,
                                          Nreal=validate, regparam=regparam,
                                          workers=workers)
        return MRW, MRWnoise

    Nvar = params['Nvar']
    # normalize parameters
    dt = 1/N
    T = T*dt
    epsilon = epsilon*dt

    # Sampling space
    timeper = np.zeros((N))
    timeper[0:int(N/2+1)] = np.arange(0, N/2+1)*dt
    timeper[int(N/2+1):int(N)] = np.arange(-N/2+1, 0)*dt
    if params['dim'] == 1:
        # causal large scale kernel
        kernelOUT = np.zeros(N,)
        kernelOUT[0:int(N/2+1)] = np.exp(-timeper[0:int(N/2+1)]/T)
        kernelOUT = fft(kernelOUT, axes=0, workers=workers)
        # multiplicative chaos : FOU with  H =0
        H0 = 0
        mykernelFOU = np.zeros((N,))
        mykernelFOU[0:int(N/2+1)] = ((H0-.5) /
                                     (timeper[0:int(N/2+1)]
                                      + epsilon)**(3/2-H0))
        # temp = np.fft.fft(mykernelFOU)
        temp = fft(mykernelFOU, axes=0, workers=workers)
        temp = temp-temp[0]
        # synthesis of the correlated noises
        if Nvar > 1:
            MyW = np.dot(np.random.randn(N, Nvar), params['Uppmf'])*np.sqrt(dt)
        else:
            MyW = np.random.randn(N, Nvar)*np.sqrt(dt)
        # myFOUnoise = temp[:, np.newaxis]*dt*np.fft.fft(MyW, axis=0)
        myFOUnoise = temp[:, np.newaxis]*dt*fft(MyW, axes=0, workers=workers)
        MyV0 = ifft(kernelOUT[:, np.newaxis]*myFOUnoise, axes=0,
                    workers=workers).real
        # take the exponential
        var = params['lambda2']*np.mean(MyV0**2, axis=0)/2
        MyW = np.exp(np.sqrt(params['lambda2'][np.newaxis, :]) * MyV0
                     - var[np.newaxis, :])
        # rint(np.std(MyW, axis=0), np.mean(MyW, axis=0))

        # the FOU process with param H
        if Nvar > 1:
            noise = np.dot(np.random.randn(N, Nvar), params['Upp'])*np.sqrt(dt)
        else:
            noise = np.random.randn(N, Nvar)*np.sqrt(dt)

        myFOUnoise = fft(MyW * noise, axes=0, workers=workers)

        mykernelFOU = np.zeros((N, Nvar))
        mykernelFOU[0:int(N/2+1), :] = ((params['H'][np.newaxis, :]-.5) /
                                        (timeper[0:int(N/2+1)][:, np.newaxis]
                                         + epsilon)**(3/2-params['H']
                                                      [np.newaxis, :]))

        temp = fft(mykernelFOU, axes=0, workers=workers)
        isHinf = np.where(params['H'] < 1/2)[0]
        temp0 = temp[0, isHinf]
        temp[:, isHinf] = (temp[:, isHinf] - temp0[np.newaxis, :])*dt

        isHsup = np.where(params['H'] >= 1/2)[0]
        temp[:, isHsup] = (temp[:, isHsup]
                           + epsilon**(params['H'][np.newaxis, isHsup]-1/2))
        MyV = ifft(kernelOUT[:, np.newaxis] * temp*myFOUnoise, axes=0,
                   workers=workers).real
        # MyV = MyV / np.std(MyV)
        # print(np.std(MyV, axis=0))
    else:
        print('Not implemented')
        MyV = []

    return MyV


# %%
def synthMultiFractalField(dim, N, H, lambda2, rho=None, rhomf=None,
                           Ani=[1, 0], regparam=0, workers=1, validate=False):
    """
        Synthesis of stationary scalar processes (1D, 2D)
        with scale invariant properties in the inertial range
        (epsilon < tau < T). The scaling can be monofractal (sigma2=0) or
        multifractal (lambda2>0). The exponent of the structure functions are

            zeta(q)=q*(H+lambda2/2) -q**2*lambda2/2

        for lambda2 > d/2 (for existence of zeta(4))

        For scale below epsilon, there is no intermittency
        and the scaling exponent is 1.
        For scale larger than T, there is a constant behavior.

    MRW = synthMultiFractalField(dim, N, H, lambda2,
                                 rho=None, rhomf=None, Ani=[1, 0],
                                 egparam=0, workers=1, validate=False)

    Parameters
    ----------
    dim: int.
        dimension of the process to be generated.
     N : int
       size of the process.
     H : array of float of size(Nvar,)
       Contains the Hurst exponent (0<H<1) of each signal to be generated.
     lambda2 : array of float of size(Nvar,)
       Contains the intermittency parameter of each signal to be generated.
       Must have the same length as H. The default is 0.025.
     rho : array of float of size (Nvar, Nvar), optional
        For multivariate synthesis only.
        Contains the correlation matrix of the noise used in the generation
        of the FGN. The default is None.
     rhomf : array of float of size (Nvar, Nvar), optional
        For multivariate synthesis only.
        Contains the correlation matrix of the noise used in the generation
        of the multiplicative chaos. The default is None.
     Ani : list of size 2 or 4, optional
         Anisotropic parameters  used if dim =2
         Ani=[alphaSS, thetaSS] where
            0 < alphaSS < 2. For alphaSS != 1,
                the space is dilated differently for x and y direction)
            0 < thetaSS < 2*pi. Angle of the generating base.
         The deformation of the space are the same for the two noises.
         if Ani list of size 4 the anisotropic parameters for the two noises.
         Ani=[alphaSS, thetaSS, alphaMF, thetaMF]
         The default is Ani =[1, 0].
     regparam : list of size 2, optional
        regularisation parameters regparam=[epsilon, T] where epsilon is the
        disspation scale and T is the integral scale.
        1/2 < epsilon < T <N
        The default is [2, N//4].
     workers : int, optional.
        Number of threads to use in the FFT's. The default is 1.
     validate : int, optional
        Contains the number of signal to analyse. If superior to 0 the
        function display an analysis of the generated signals.
        The default is 0.

    Raises
    ------
    TypeError
        if the input parameters are not well choosen.

    References
    ----------
    For the multilicative chaos :
      RHODES, R. & VARGAS, V.
      Gaussian multiplicative chaos and applications: A review.
      Probability Surveys 11, 315–392. 2014.

    For the multifractal field (in 3d):
      R.M. Pereira, C. Garban, L. Chevillard,
      A dissipative random velocity field for fully developed fluid turbulence.
      J. Fluid Mech. 794, 369 (2016).

    Returns
    -------
    MRW : array of float of size(N,Nvar) or (N,N, Nvar)
        The generated process(es).


    ##
    S.G.  Roux, ENS Lyon, May 2021

    """

    regparam = np.atleast_1d(regparam)
    # check regparam
    test = True
    if regparam[0] == 0:
        epsilon = 2
        T = N/4
    elif regparam[0] > regparam[1] and regparam[1] < 1:
        test = False
        print('epsilon must be an integer between 1 and T.\n')
    else:
        epsilon = max(regparam[0], 0.5)
        T = int(regparam[1])
    if not test:
        raise TypeError('Wrong input parameters.\n')

    # check parameters
    params = checkparams(dim, N, H, lambda2, rho, rhomf, Ani)
    if params['dim'] == 0:
        raise TypeError('Wrong input parameters.\n')

    if validate > 0:
        MRW, MRWnoise = validatesynthesis(synthMultiFractalField,
                                          dim, N, H, lambda2,
                                          rho=rho, rhomf=rhomf, Ani=Ani,
                                          Nreal=validate, regparam=regparam,
                                          workers=workers)
        return MRW, MRWnoise

    Nvar = params['Nvar']
    # normalize parameters
    dt = 1/N
    T = T*dt
    epsilon = epsilon*dt

    # Sampling space
    timeper = np.zeros((N))
    timeper[0:int(N/2+1)] = np.arange(0, N/2+1)*dt
    timeper[int(N/2+1):int(N)] = np.arange(-N/2+1, 0)*dt

    if params['dim'] == 1:

        n2 = np.abs(timeper)
        n2[n2 > T] = T
        # ---- Multiplicatif chaos
        RegulNorm = np.sqrt(n2**2+epsilon**2)
        mycorr = np.zeros((Nvar, N))
        for ivar in range(Nvar):
            mycorr[ivar, :] = params['lambda2'][ivar] * np.log(T/RegulNorm)
        L2 = fft(mycorr, axes=1, workers=workers)

        # synthesis of the correlated noises
        if Nvar > 1:
            wtemp = np.dot(np.random.randn(N, Nvar), params['Uppmf'])
        else:
            wtemp = np.random.randn(N, Nvar)
        # TF of the noise
        w = fft(wtemp, axes=0, workers=workers)
        x2 = np.real(ifft(w*np.sqrt(L2.transpose()), axes=0, workers=workers))

        Xr = np.exp(x2.real)
        with warnings.catch_warnings():
            warnings.simplefilter("ignore", category=RuntimeWarning)
            Xr = Xr/np.std(Xr)
        i0 = np.where(params['lambda2'] == 0)[0]
        Xr[:, i0] = 1

        # fbm
        # regularized norm
        Repsilon = np.sqrt(timeper**2+epsilon**2)
        # large scale function
        phiL = 1/np.sqrt(2*np.pi*T**2)*np.exp(-(timeper**2)/2/T**2)
        # scale invariant kernel
        mykernelFOU1d = np.zeros((Nvar, N))
        for ivar in range(Nvar):
            mykernelFOU1d[ivar, :] = (phiL*Repsilon
                                      / (Repsilon) ** (3/2-params['H'][ivar]))

        # synthesis of the correlated noises
        if Nvar > 1:
            MyW1d = np.dot(np.random.randn(N, Nvar), params['Upp'])*dt
        else:
            MyW1d = np.random.randn(N, Nvar)*dt

        # synthesis
        Mvt = ifft(fft(mykernelFOU1d.transpose(), axes=0, workers=workers) *
                   fft(MyW1d*Xr, axes=0, workers=workers), axes=0,
                   workers=workers).real

    elif params['dim'] == 2:
        n2 = np.abs(timeper)
        [X, Y] = np.meshgrid(n2, n2)
        if params['thetaSS'] > 0:
            X1 = np.cos(params['thetaSS'])*X-np.sin(params['thetaSS'])*Y
            Y1 = np.sin(params['thetaSS'])*X+np.cos(params['thetaSS'])*Y
        else:
            X1 = X
            Y1 = Y

        # dilation of the space
        R2 = (np.abs(X1)**(2/params['alphaSS'])
              + np.abs(Y1)**(2/(2-params['alphaSS'])))
        # ---------------
        # R2 = X**2 + Y**2

        R2[np.sqrt(R2) > 0.5] = 0.5**2
        R = np.sqrt(R2+epsilon**2)
        # new ones
        # Multiplicatif chaos
        # n2 = np.abs(timeper)
        n2[n2 > T] = T
        [Xb, Yb] = np.meshgrid(n2, n2)
        if params['thetaMF'] > 0:
            X2 = np.cos(params['thetaMF'])*Xb-np.sin(params['thetaMF'])*Yb
            Y2 = np.sin(params['thetaMF'])*Xb+np.cos(params['thetaMF'])*Yb
        else:
            X2 = Xb
            Y2 = Yb
        # Regul norm
        RegulNorm2 = (np.abs(X2)**(2/params['alphaMF'])
                      + np.abs(Y2)**(2/(2-params['alphaMF'])))
        # --------
        # RegulNorm2 = Xb**2 + Yb**2

        RegulNorm = np.sqrt(RegulNorm2+epsilon**2)
        RegulNorm[RegulNorm2 > T] = T

        mycorr = np.log(T/RegulNorm)
        L2 = np.zeros((Nvar, N, N))+1j * np.zeros((Nvar, N, N))
        for ivar in range(Nvar):
            # L2[ivar, :, :] = np.fft.fft2(params['lambda2'][ivar]*mycorr)
            L2[ivar, :, :] = fft(params['lambda2'][ivar]*mycorr, axes=(0, 1),
                                 workers=workers)
        # transpose results
        L2 = np.transpose(L2, (1, 2, 0))
        # generation of the correlated noises
        wtemp = np.random.randn(N, N, Nvar)
        if Nvar > 1:
            w = np.dot(wtemp.reshape(N**2, Nvar), params['Upp'])
            ww = w.reshape(N, N, Nvar)
        else:
            ww = wtemp
        # w = np.fft.fft(np.fft.fft(ww, axis=0), axis=1)
        w = fft(ww, axes=(0, 1), workers=workers)
        # x2 = np.fft.ifft(np.fft.ifft(w*np.sqrt(L2), axis=1), axis=0)
        x2 = ifft(w*np.sqrt(L2), axes=(0, 1))
        Xr = np.exp(x2.real)
        if params['lambda2'][ivar] > 0:
            Xr = Xr/np.std(Xr)
        else:
            Xr = np.ones(Xr.shape)

        # large scale function
        # phiL = 1/np.sqrt(2*np.pi*T**2)*np.exp(-R2/2/T**2)
        phiL = 1/np.sqrt(2*np.pi*T**2)*np.exp(-(X**2+Y**2)/2/T**2)
        # tmp = (np.abs(X1)**(2/params['alphaSS'])
        #        + np.abs(Y1)**(2/(2-params['alphaSS'])))
        # phiL = 1/np.sqrt(2*np.pi*T**2)*np.exp(-tmp/2/T**2)

        # generation of the correlated noises
        wtemp = np.random.randn(N, N, Nvar)*dt
        if Nvar > 1:
            w = np.dot(wtemp.reshape(N**2, Nvar), params['Upp'])
            ww = w.reshape(N, N, Nvar)
        else:
            ww = wtemp
        # MyW2d = np.fft.fft(np.fft.fft(ww*Xr, axis=0), axis=1)
        MyW2d = fft(ww*Xr, axes=(0, 1), workers=workers)

        # invariant kernel
        mykernelFOU2d = np.zeros((N, N, Nvar))
        for ivar in range(Nvar):
            mykernelFOU2d[:, :, ivar] = (
                phiL*R)/R**(1+params['dim']/2-params['H'][ivar])

        # Mvt = np.fft.ifft(np.fft.ifft(
        #     np.fft.fft(np.fft.fft(mykernelFOU2d, axis=0), axis=1)
        #     * MyW2d, axis=1), axis=0).real
        Mvt = ifft(fft(mykernelFOU2d, axes=(0, 1), workers=workers) *
                   MyW2d, axes=(0, 1), workers=workers).real

    else:
        print('Not yet implemented')
        Mvt = []

    return Mvt


# %%
if __name__ == '__main__':

    # %% test mono varie regul
    dim = 1
    N = 2**15
    Nreal = 5
    H = 0.7
    lambda2 = 0.025
    MRW = synthMultiFractalField(dim, N, H, lambda2)
    GRRRRR
    # %% Test validation 1D  multivarite MRW
    # parameters
    dim = 1
    N = 2**15
    Nreal = 5
    rho = scspatial.distance.squareform(0.8)+np.eye(2)
    rhomf = scspatial.distance.squareform(0)+np.eye(2)
    H = [0.7, 0.2]
    lambda2 = [0.05, 0.02]

    # test monovarie
    MRW, MRWnoise = synthmrw(dim, N, H[0], lambda2[0], validate=Nreal)

    # %% test bivarie
    MRW, MRWnoise = synthmrw(dim, N, H, lambda2, rho, rhomf, validate=Nreal)

    # %% test 4 composantes
    H = [0.8, 0.3, 0.4, 0.7]
    lambda2 = [0.05, 0.05, 0.03, 0.03]
    rho = [[1, 0.5, -0.4, 0.3], [0.5, 1, 0.4, -0.2],
           [-0.4, 0.4, 1, 0.05], [0.3, -0.2, 0.05, 1]]
    rhomf = [[1, -0.3, 0.3, 0.2], [-0.3, 1, -0.15, 0.15],
             [0.3, -0.15, 1, 0], [0.2, 0.15, 0, 1]]

    MRW, MRWnoise = synthmrw(dim, N, H, lambda2, rho, rhomf, validate=Nreal)

    # %% Test validation 2D  multivarite MRW
    # parameters
    dim = 2
    N = 2**8
    Nreal = 3
    rho = scipy.spatial.distance.squareform(0.8)+np.eye(2)
    rhomf = scipy.spatial.distance.squareform(0)+np.eye(2)
    H = [0.7, 0.2]
    lambda2 = [0.05, 0.02]

    # monovarie
    MRW, MRWnoise = synthmrw(dim, N, H[0], lambda2[0], validate=Nreal)

    # %%  bivarie
    MRW, MRWnoise = synthmrw(dim, N, H, lambda2, rho, rhomf, validate=Nreal)

    # %% test 4 composantes
    H = [0.8, 0.3, 0.4, 0.7]
    lambda2 = [0.05, 0.05, 0.03, 0.03]
    rho = [[1, 0.5, -0.4, 0.3], [0.5, 1, 0.4, -0.2],
           [-0.4, 0.4, 1, 0.05], [0.3, -0.2, 0.05, 1]]
    rhomf = [[1, -0.3, 0.3, 0.2], [-0.3, 1, -0.15, 0.15],
             [0.3, -0.15, 1, 0], [0.2, 0.15, 0, 1]]

    MRW, MRWnoise = synthmrw(dim, N, H, lambda2, rho, rhomf, validate=Nreal)

    # %%
    from os import environ

    N_threads = '4'
    environ['NUMEXPR_NUM_THREADS'] = N_threads
    environ['MKL_NUM_THREADS'] = N_threads
    environ['OMP_NUM_THREADS'] = N_threads

    N = 1024
    x = np.random.randn(N, N)
    t = time.time()
    for _ in range(1000):
        np.multiply(x, x)

    print(time.time()-t)

    N_threads = '1'
    environ['NUMEXPR_NUM_THREADS'] = N_threads
    environ['MKL_NUM_THREADS'] = N_threads
    environ['OMP_NUM_THREADS'] = N_threads
    # %%
    import numpy as np
    import time
    import scipy
    import pyfftw
    pyfftw.interfaces.cache.enable()
    pyfftw.interfaces.cache.set_keepalive_time(60)
    N = 2**13
    x = np.random.randn(N, N)
    t = time.time()
    for ii in range(10):
        np.fft.fftn(x)
    print('numpy (fftn) : ', time.time()-t)

    # %% 2d
    import numpy as np
    import time
    import scipy
    N = 2**12
    x = np.random.randn(N, N)
    for workers in range(1, 5):
        t = time.time()
        for ii in range(10):
            scipy.fft.fftn(x, axes=(0, 1), workers=workers)
        print('scipy (fftn) : {:d} threads'.format(workers), time.time()-t)
    for workers in range(1, 5):
        for ii in range(10):
            scipy.fft.fft2(x, axes=(0, 1), workers=workers)
        print('scipy (fft2) : {:d} threads'.format(workers), time.time()-t)
    # %% 1d
    import numpy as np
    import time
    import scipy
    N = 2**24
    x = np.random.randn(N)
    t = time.time()
    for ii in range(10):
        np.fft.fftn(x)
    print('numpy (fftn) : ', time.time()-t)
    for workers in range(1, 5):
        t = time.time()
        for ii in range(10):
            scipy.fft.fftn(x, axes=(0), workers=workers)
        print('scipy (fftn) : {:d} threads'.format(workers), time.time()-t)
    for workers in range(1, 5):
        for ii in range(10):
            scipy.fft.fft2(x, axes=(0), workers=workers)
        print('scipy (fft2) : {:d} threads'.format(workers), time.time()-t)

    # %%
    for workers in range(2, 5):
        t = time.time()
        for ii in range(10):
            pyfftw.interfaces.numpy_fft.fft2(x, axes=(0, 1), threads=workers)
        print('fftw (fft2) : {:d} threads'.format(workers), time.time()-t)
    for workers in range(2, 5):
        a = pyfftw.empty_aligned((N, N), dtype='float')
        fftp = pyfftw.builders.fftn(a, axes=(0, 1), threads=workers)
        t = time.time()
        for ii in range(10):
            a[:] = x
            fftp()
        print('fftw builder (fftn) : {:d} threads'.format(workers),
              time.time()-t)

    # %%
    import time
    t = time.time()
    for i in range(100):
        MRW = synthMFOU(2**16, H, lambda2, rho=0, rhomf=0, Ani=[1, 0],
                        workers=1)
    print(time.time()-t)

    # %%
    N = 2**10
    Nvar = 2
    nv = 5
    Nreal = 5
    uu = 2**np.arange(0, np.log2(N)-3, 1/nv)
    scales = np.unique(uu.astype(int))
    Mom2 = np.zeros((Nreal, len(scales), Nvar, 3))
    Mom4 = np.zeros((Nreal, len(scales), Nvar, 3))
    Mom2_0 = np.zeros((Nreal, len(scales), 3))
    Mom4_0 = np.zeros((Nreal, len(scales), 3))
    Mom2_1 = np.zeros((Nreal, len(scales), 3))
    Mom4_1 = np.zeros((Nreal, len(scales), 3))
    for ireal in range(Nreal):
        print(ireal)
        MRW,  MRWnoise = synthmrw(2, N, [.3, .7], [0.05, 0.025], workers=4)
        Mom2[ireal, :,:,:], Mom4[ireal, :,:,:], scales = analyseIncrSimple(MRW, nv=nv, dim=2, workers=4)
        Mom2_0[ireal, :,:], Mom4_0[ireal, :,:], scales = analyseIncrSimple(MRW[:,:,0], nv=nv, dim=2, workers=4)
        Mom2_1[ireal, :,:], Mom4_1[ireal, :,:], scales = analyseIncrSimple(MRW[:,:,1], nv=nv, dim=2, workers=4)


    M2= np.mean(Mom2,axis=0)
    M4= np.mean(Mom4,axis=0)
    M2_0= np.mean(Mom2_0,axis=0)
    M4_0= np.mean(Mom4_0,axis=0)
    M2_1= np.mean(Mom2_1,axis=0)
    M4_1= np.mean(Mom4_1,axis=0)
    plt.figure(1)
    plt.clf()
    plt.subplot(221)
    plt.plot(np.log(scales), np.log(M2[:,0,0]), 'k.')
    plt.plot(np.log(scales), np.log(M2[:,0, 1]), 'k')
    plt.plot(np.log(scales), np.log(M2[:,0,2]), 'k+')
    
    plt.plot(np.log(scales), np.log(M2[:,1,0]), 'r--')
    plt.plot(np.log(scales), np.log(M2[:,1,1]), 'r-')
    plt.plot(np.log(scales), np.log(M2[:,1,1]), 'r.')
    
    
    plt.subplot(222)
    plt.plot(np.log(scales), np.log(M2_0[:,0]), 'k.')
    plt.plot(np.log(scales), np.log(M2_0[:,1]), 'k')
    plt.plot(np.log(scales), np.log(M2_0[:,2]), 'k+')
    
    plt.plot(np.log(scales), np.log(M2_1[:,0]), 'r.')
    plt.plot(np.log(scales), np.log(M2_1[:,0]), 'r')
    plt.plot(np.log(scales), np.log(M2_1[:,0]), 'r+')
    
    plt.subplot(223)
    plt.plot(np.log(scales), np.log(M4[:,0, 0]/M2[:,0, 0]**2/3), 'k.')
    plt.plot(np.log(scales), np.log(M4[:,0,1]/M2[:,0, 1]**2/3), 'k')
    plt.plot(np.log(scales), np.log(M4[:,0,2]/M2[:,0, 2]**2/3), 'k+')

    plt.plot(np.log(scales), np.log(M4[:, 1,0]/M2[:,1, 0]**2/3),'r.')
    plt.plot(np.log(scales), np.log(M4[:, 1, 1]/M2[:,1,1]**2/3), 'r')
    plt.plot(np.log(scales), np.log(M4[:, 1, 2]/M2[:,1,2]**2/3), 'r+')
    
    
    plt.subplot(224)
    plt.plot(np.log(scales), np.log(M4_0[:,0]/M2_0[:,0]**2/3), 'k.')
    plt.plot(np.log(scales), np.log(M4_0[:,1]/M2_0[:,1]**2/3), 'k')
    plt.plot(np.log(scales), np.log(M4_0[:,2]/M2_0[:,2]**2/3), 'k+')

    plt.plot(np.log(scales), np.log(M4_1[:, 0]/M2_1[:,0]**2/3),'r.')
    plt.plot(np.log(scales), np.log(M4_1[:, 1]/M2_1[:,1]**2/3), 'r')
    plt.plot(np.log(scales), np.log(M4_1[:, 2]/M2_1[:,2]**2/3), 'r+')
    