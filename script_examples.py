#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 19 20:02:51 2023

@author: sroux
"""

import MFsynthesis as MF
import matplotlib.pyplot as plt

if __name__ == '__main__':

    print('First create 1 dimensional multifractal processes.')
    dim = 1  # dimension of the field
    N = 2**16  # size of the realisation
    H = 0.3  # Hurst exponent
    c2 = 0.05  # intermittency parameters
    workers = 4  # number of threads use in the FFT's
    # a non stationary
    MRWn, MRWnoise = MF.synthmrw(dim, N, H, c2, workers=4)

    # a stationary one :
    # we need to define a large scale T and a dissipative scale epsilon
    epsilon = 1
    T = N//4
    MRWs = MF.synthMultiFractalField(dim, N, H, c2, regparam=[epsilon, T],
                                     workers=4)
    plt.figure(1)
    plt.clf()
    plt.subplot(211)
    plt.plot(MRWn)
    plt.title('Non stationary process')
    plt.show()
    plt.subplot(212)
    plt.plot(MRWs)
    plt.title('stationary process')
    plt.show()
    # %% if you want to see the characteristics of the process synthetized
    # use the parameter validate to give a number of realisation from which
    # we compute and display statistics
    Nreal = 5
    MRWs = MF.synthMultiFractalField(dim, N, H, c2, regparam=[epsilon, T],
                                     workers=workers, validate=Nreal)
    
    # %%Multivariate one dimensional process
    rho = [[1, .3],
           [.3, 1]]  # correlation of the invariant noise
    rhomf = [[1, .7],
             [.7, 1]]   # correlation of the gaussian multiplicative chaos
    MRWn, MRWnoise = MF.synthmrw(dim, N, H, c2, rho=rho, rhomf=rhomf,
                                 workers=workers)
    print(MRWn.shape)

    # %%
    print('Second create 2 dimensional multifractal processes.')
    dim = 2  # dimension of the field
    N = 2**10
    MRWn, MRWnoise = MF.synthmrw(dim, N, H, c2, workers=4)

    # a stationary one :
    # we need to define a large scale T and a dissipative scale epsilon
    epsilon = 1
    T = N//4
    MRWs = MF.synthMultiFractalField(dim, N, H, c2, regparam=[epsilon, T],
                                     workers=workers)
    plt.figure(2)
    plt.clf()
    plt.subplot(121)
    plt.imshow(MRWn)
    plt.title('Non stationary process')
    plt.show()
    plt.subplot(122)
    plt.imshow(MRWs)
    plt.title('stationary process')
    plt.show()

    # %%
    print('Third create multivariate 2 dimensional multifractal processes.')
    dim = 2  # dimension of the field
    N = 2**10
    # the Hurst exponent is now a list of length the number of varaibles
    H = [.7, .3]
    # the intermittency exponent is a list with same size as H
    c2 = [0.05, 0.025]
    # we need to define the dependance between the variables. As the synthesis
    # of one variable rely on two noises, we can prescribe two covariance
    # matrix. These matrix must be square, symmetric and have a size equal to
    # the number of variable

    rho = [[1, .3],
           [.3, 1]]  # correlation of the invariant noise
    rhomf = [[1, .7],
             [.7, 1]]  # correlation of the gaussian multiplicative chaos
    MRWn, MRWnoise = MF.synthmrw(dim, N, H, c2, rho=rho, rhomf=rhomf,
                                 workers=workers)

    # a stationary one :
    # we need to define a large scale T and a dissipative scale epsilon
    epsilon = 1
    T = N//4
    MRWs = MF.synthMultiFractalField(dim, N, H, c2, rho=rho, rhomf=rhomf,
                                     regparam=[epsilon, T], workers=workers)
    plt.figure(2)
    plt.clf()
    plt.subplot(221)
    plt.imshow(MRWn[:, :, 0])
    plt.title('Non stationary : component 0')
    plt.show()
    plt.subplot(222)
    plt.imshow(MRWn[:, :, 1])
    plt.title('Non stationary : component 1')
    plt.show()
    plt.subplot(223)
    plt.imshow(MRWs[:, :, 0])
    plt.title('stationary : component 0')
    plt.show()
    plt.subplot(224)
    plt.imshow(MRWs[:, :, 1])
    plt.title('stationary : component 1')
    plt.suptitle('Bivariate case')
    plt.show()

    # %%
    print('Fourth create anisotropic multifractal fields.')
    dim = 2  # dimension of the field
    N = 2**10
    # the Hurst exponent is now a list of length the number of varaibles
    H = [.7, .3]
    # the intermittency exponent is a list with same size as H
    c2 = [0.05, 0.025]
    # we need a parameter for the anisotropic deformation of the space.
    # The deformation implemented is X --> X**\alpha, Y --> Y **(2-alpha)
    # with 0 < alpha < 2. alpha = 1 correspond to the isotropic case.
    alpha = 0.7
    theta = 0  # the angle of the base (X, Y) with (horyzontal, vertical) axes.

    dim = 2  # dimension of the field
    N = 2**10
    # the Hurst exponent is now a list of length the number of varaibles
    H = [.7, .3]
    # the intermittency exponent is a list with same size as H
    c2 = [0.05, 0.025]
    MRWn, MRWnoise = MF.synthmrw(dim, N, H, c2, Ani=[alpha, theta], workers=workers)

    # a stationary one :
    # we need to define a large scale T and a dissipative scale epsilon
    epsilon = 1
    T = N//4
    MRWs = MF.synthMultiFractalField(dim, N, H, c2, regparam=[epsilon, T],
                                     Ani=[alpha, theta], workers=workers)
    plt.figure(2)
    plt.clf()
    plt.subplot(221)
    plt.imshow(MRWn[:, :, 0])
    plt.title('Non stationary : component 0')
    plt.show()
    plt.subplot(222)
    plt.imshow(MRWn[:, :, 1])
    plt.title('Non stationary : component 1')
    plt.show()
    plt.subplot(223)
    plt.imshow(MRWs[:, :, 0])
    plt.title('stationary : component 0')
    plt.show()
    plt.subplot(224)
    plt.imshow(MRWs[:, :, 1])
    plt.title('stationary : component 1')
    plt.suptitle('Bivariate case')
    plt.show()
