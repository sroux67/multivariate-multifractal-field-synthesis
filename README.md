# Multivariate Multifractal Field Synthesis

## Name
Multivariate Multifractal Field Synthesis.

## Description
Multivariate Multifractal Field Synthesis is a python library for synthesis of **1 dimensional** and **two dimensional multivariate** and **multifractal** scalar fields either **isotropic** or **anisotropic**.

This code can generate stationary and non stationnary processes $v$
with a power spectrum behaving as a power law of the frequencies. 
Different scaling laws can be prescribed to generate processes caracterized by linear or non linear structure functions behavior such that : 
$$\mathbb{E}\{(v(t+\tau)-v(t))^q\} \sim_{\tau\rightarrow 0} \tau^{\zeta(q)}$$
with a quadratic 
$$\zeta(q)=c_1 q - c_2\frac{q^2}{2},$$ 
with $c_1 \in [0, 1[$ and $c_2\leq0$.

For $c_2=0$ the process is monofractal as a Fractional Brownian Motion.

For $c_2 > 0$ the process is monofractal as a Fractional  the process is  multifractal as a Multifractal Random Walk.

## Visuals
Isotropic fields
![isotropic field](isotropic.png)
Anisotropic fields
![anisotropic field](anisotropic.png)

## Installation
Download the files and import the librarie MFsynthesis.py in python :
`import MFsynthesis`

This file import several common libraries : time, warnings, numpy, scipy, matplotlib, functools and 
concurrent.futures. 


## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Authors and acknowledgment
Stéphane Roux, Ecole Normale Supérieure de Lyon, Laboratoire de physique, UMR 5672.

## License
 Copyright (C) 2023 GNU AGPLv3, Multivariate Multifractal Field Synthesis 2023, S.G. Roux.
